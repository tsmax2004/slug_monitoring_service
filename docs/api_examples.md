# API examples

### 1. Метод создания сегмента

Название сегмента должно состоять из <=64 символов, не должно содержать символ ";".
Опициональное поле "percent" принимает float от 0.0 до 1.0, указывающий, какая часть имеющихся
в базе данных пользователей автоматически добавятся в новый сегмент.

```bash
curl -X 'POST' \
  'http://localhost:8080/slugs' \
  -i \
  -d '{
  "slug": { "name": "AVITO_VOICE_MESSAGES" },
  "percent": 0.5
}'
```

- Ответ при удачной регистрации сегмента:
```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Date: Mon, 28 Aug 2023 09:22:39 GMT
Content-Length: 38

{
    "name": "AVITO_VOICE_MESSAGES"
}
```

- Ответ при попытке регистрации уже имеющегося сегмента:
```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8
Date: Mon, 28 Aug 2023 09:26:42 GMT
Content-Length: 61

{
    "message": "slug AVITO_VOICE_MESSAGES already exists"
}
```

### 2. Метод удаления сегмента

```bash
curl -X 'DELETE' \
  'http://localhost:8080/slugs' \
  -i \
  -d '{
  "name": "AVITO_VOICE_MESSAGES"
}'
```

- Ответ (не зависит от того, зарегистрирован ли переданный сегмент):

```
HTTP/1.1 200 OK
Date: Mon, 28 Aug 2023 09:31:29 GMT
Content-Length: 0 
```

### 3. Метод обновления сегментов пользователя

Добавляет пользователя в сегменты, перечисленные в `add` (если он уже состоит в каком-то из них, то этот сегмент пропускается).
Удаляет пользователя из сегментов, перечисленных в `del`. В ответе сообщает о незарегистрированных сегментах.

В качестве необязательного параметра `expired` можно передать дату и время автоматического удаления
сегмента у пользователя в формате YYYY-MM-DD hh:mm:ss

```bash
curl -X 'PUT' \
  'http://localhost:8080/link' \
  -i \
  -d '{
  "user": {
    "id": 1000
  },
  "add": [
    {
      "name": "AVITO_VOICE_MESSAGES",
      "expired": "2023-09-01 00:00:00"
    },
    {
      "name": "AVITO_PERFORMANCE_VAS"
    },
    {
      "name": "UNREGISTERED_SLUG_1"
    }
  ],
  "del": [
    {
      "name": "UNREGISTERED_SLUG_2"
    }
  ]
}' 
```

- Ответ:
```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Date: Mon, 28 Aug 2023 09:34:56 GMT
Content-Length: 76

{
    "message": "slugs UNREGISTERED_SLUG_1 UNREGISTERED_SLUG_2 not exist"
}
```

### 4. Метод получения активных сегментов пользователя

```bash
curl -X 'GET' \
  'http://localhost:8080/link' \
  -i \
  -d '{
  "id": 1000
}'
```

- Ответ:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Date: Mon, 28 Aug 2023 09:39:45 GMT
Content-Length: 107

[
    {
        "name": "AVITO_PERFORMANCE_VAS"
    },
    {
        "name": "AVITO_VOICE_MESSAGES"
    }
]
```

### 5. Метод получения истории обновления сегментов пользователей

from - дата, с которой будут предоставлены данные, в формате YYYY-MM

```bash
curl -X 'GET' \
  'http://localhost:8080/history' \
  -i \
  -d '{
  "from": "2023-01"
}'
```

- Ответ (csv-файл):

```
1000;AVITO_VOICE_MESSAGES;del;2023-08-28 12:27:30.857662 +0000 UTC
1000;AVITO_VOICE_MESSAGES;add;2023-08-28 12:27:30.850827 +0000 UTC
1000;AVITO_VOICE_MESSAGES;del;2023-08-28 12:27:21.057925 +0000 UTC
1000;AVITO_VOICE_MESSAGES;add;2023-08-28 12:26:22.504227 +0000 UTC
```