package main

import (
	"encoding/csv"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgx/v5/pgxpool"
	"log"
	"net/http"
	"os"
	"sms/models"
	"sms/postgres"
	"strconv"
	"time"
)

const (
	ttlPipeBufSize = 4096
)

type Server struct {
	db      *pgxpool.Pool
	ttlPipe chan time.Time
}

func CreateServer() (*Server, error) {
	server := Server{ttlPipe: make(chan time.Time, ttlPipeBufSize)}

	db, err := postgres.CreatePostgresClient()
	if err != nil {
		return nil, err
	}
	server.db = db

	go server.ttlCycle()
	return &server, nil
}

// -----------------------------------------------------------------------------
// TTL timer
// -----------------------------------------------------------------------------

func (s *Server) ttlCycle() {
	next, err := postgres.DeleteExpiredLinks(s.db)
	if errors.As(err, &postgres.ErrNoRows{}) {
		next = postgres.MaxDatetimeValue
	} else if err != nil {
		log.Println(err.Error())
		return
	}
	timer := time.NewTimer(time.Until(next))

	for {
		select {
		case <-timer.C:
			next, err = postgres.DeleteExpiredLinks(s.db)
			if errors.As(err, &postgres.ErrNoRows{}) {
				next = postgres.MaxDatetimeValue
			} else if err != nil {
				log.Println(err.Error())
				return
			}
			timer.Reset(time.Until(next))

		case newNext, ok := <-s.ttlPipe:
			if !ok {
				return
			}
			if newNext.Before(next) {
				timer.Reset(time.Until(newNext))
				next = newNext
			}
		}
	}
}

// -----------------------------------------------------------------------------
// HANDLERS
// -----------------------------------------------------------------------------

func (s *Server) postSlug(c *gin.Context) {
	var req struct {
		Slug    models.Slug `json:"slug"`
		Percent float64     `json:"percent"`
	}

	if err := c.BindJSON(&req); err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	err := postgres.AddSlug(s.db, &req.Slug)
	if err != nil {
		switch err.(type) {
		case *postgres.SlugAlreadyExist:
			c.IndentedJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		default:
			c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		}
		return
	}
	c.IndentedJSON(http.StatusCreated, req.Slug)

	if 0.0 < req.Percent && req.Percent <= 1.0 {
		if err := postgres.AutoAddSlugToUsers(s.db, &req.Slug, req.Percent); err != nil {
			log.Println(err.Error())
		}
	}
}

func (s *Server) deleteSlug(c *gin.Context) {
	var slugToDelete models.Slug
	if err := c.BindJSON(&slugToDelete); err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	if err := postgres.DeleteSlug(s.db, slugToDelete); err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
	}
}

func (s *Server) updateUserSlugs(c *gin.Context) {
	var req struct {
		User models.User    `json:"user"`
		Add  []*models.Slug `json:"add"`
		Del  []*models.Slug `json:"del"`
	}

	if err := c.BindJSON(&req); err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	earliestExpired := postgres.MaxDatetimeValue
	for _, sl := range req.Add {
		sl.ExpiredDt = postgres.MaxDatetimeValue
		if len(sl.ExpiredStr) > 0 {
			var err error
			sl.ExpiredStr += " +0300"
			if sl.ExpiredDt, err = time.Parse("2006-01-02 15:04:05 -0700", sl.ExpiredStr); err != nil {
				c.IndentedJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
				return
			}
			sl.ExpiredDt = sl.ExpiredDt.UTC()
			if sl.ExpiredDt.Before(earliestExpired) {
				earliestExpired = sl.ExpiredDt
			}
		}
	}

	err := postgres.UpdateSlugsOfUser(s.db, req.User, req.Add, req.Del)
	s.ttlPipe <- earliestExpired
	if err == nil {
		c.Status(http.StatusCreated)
		return
	}

	switch err.(type) {
	case *postgres.SlugsNotExist:
		c.IndentedJSON(http.StatusCreated, gin.H{"message": err.Error()})
	default:
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
	}
}

func (s *Server) getUserSlugs(c *gin.Context) {
	var user models.User
	if err := c.BindJSON(&user); err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	slugs, err := postgres.GetUserSlugs(s.db, user)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}
	c.IndentedJSON(http.StatusOK, slugs)
}

func prepareHistory(history []*postgres.HistoryRow) [][]string {
	var res [][]string
	for _, row := range history {
		strRow := []string{strconv.Itoa(row.UserId), row.SlugName, row.Operation, row.Datetime.String()}
		res = append(res, strRow)
	}
	return res
}

func (s *Server) getHistory(c *gin.Context) {
	var req struct {
		From string `json:"from"`
	}
	if err := c.BindJSON(&req); err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	datetimeFrom, err := time.Parse("2006-01", req.From)
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}

	file, err := os.Create("history.csv")
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}
	defer file.Close()

	history, err := postgres.GetHistory(s.db, datetimeFrom)
	if err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	w := csv.NewWriter(file)
	w.Comma = ';'
	if err = w.WriteAll(prepareHistory(history)); err != nil {
		c.IndentedJSON(http.StatusInternalServerError, gin.H{"message": err.Error()})
		return
	}

	c.FileAttachment("./history.csv", "history.csv")
	c.Writer.Header().Set("attachment", "filename=history.csv")
}
