package models

import "time"

type Slug struct {
	Id   string `json:"-"`
	Name string `json:"name"`

	ExpiredStr string    `json:"expired,omitempty"`
	ExpiredDt  time.Time `json:"-"`
}
