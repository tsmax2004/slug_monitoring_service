package postgres

import (
	"context"
	"errors"
	"fmt"
	"github.com/georgysavva/scany/v2/pgxscan"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
	"math/rand"
	"sms/models"
	"time"
)

var (
	MaxDatetimeValue = time.Date(9999, 1, 1, 0, 0, 0, 0, time.UTC)
)

// PostgresClient implemented by pgxpool.Pool
type PostgresClient interface {
	Exec(ctx context.Context, sql string, arguments ...any) (pgconn.CommandTag, error)
	Query(ctx context.Context, query string, args ...interface{}) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, arguments ...any) pgx.Row
}

func CreatePostgresClient() (*pgxpool.Pool, error) {
	conn_str := fmt.Sprintf("postgresql://user:password@postgres:5432/slug_monitor_db")

	pool, err := pgxpool.New(context.Background(), conn_str)
	return pool, err
}

func fillSlugId(client PostgresClient, slug *models.Slug) error {
	q := "SELECT id FROM sms.slug WHERE name=$1"
	return client.QueryRow(context.Background(), q, slug.Name).Scan(&slug.Id)
}

// AddSlug and fill id or report that the slug with the same name is already exist
func AddSlug(client PostgresClient, slug *models.Slug) error {
	if err := fillSlugId(client, slug); err == nil {
		return &SlugAlreadyExist{slug}
	}

	q := "INSERT INTO sms.slug (name) VALUES ($1) RETURNING id"

	err := client.QueryRow(context.Background(), q, slug.Name).Scan(&slug.Id)
	return err
}

func AutoAddSlugToUsers(client PostgresClient, slug *models.Slug, percent float64) error {
	q := `SELECT DISTINCT user_id FROM sms.link`

	var users []int
	if err := pgxscan.Select(context.Background(), client, &users, q); err != nil {
		return err
	}

	q = `INSERT INTO sms.link (user_id, slug_id, expired) VALUES ($1, $2, $3) ON CONFLICT DO NOTHING`
	pick_cnt := int(float64(len(users)) * percent)
	for i := 0; i < pick_cnt; i++ {
		ind := rand.Intn(len(users))
		user := users[ind]
		if _, err := client.Exec(context.Background(), q, user, slug.Id, MaxDatetimeValue); err != nil {
			return err
		}

		users[ind], users[len(users)-1] = users[len(users)-1], users[ind]
		users = users[:len(users)-1]
	}

	return nil
}

// DeleteSlug if it exists, otherwise do nothing
func DeleteSlug(client PostgresClient, slug models.Slug) error {
	q := "DELETE FROM sms.slug WHERE name=$1"
	_, err := client.Exec(context.Background(), q, slug.Name)
	return err
}

// DeleteExpiredLinks and return next expired time
func DeleteExpiredLinks(client PostgresClient) (next time.Time, err error) {
	q := "DELETE FROM sms.link WHERE expired <= NOW() AT TIME ZONE 'utc'"
	if _, err = client.Exec(context.Background(), q); err != nil {
		return
	}

	q = "SELECT MIN(expired) FROM sms.link"
	err = client.QueryRow(context.Background(), q).Scan(&next)
	if errors.Is(err, pgx.ErrNoRows) || errors.As(err, &pgx.ScanArgError{}) {
		err = ErrNoRows{}
		return
	}
	return
}

// UpdateSlugsOfUser - add and delete, report about slugs that don't exist
func UpdateSlugsOfUser(client PostgresClient, user models.User, add []*models.Slug, del []*models.Slug) error {
	funAdd := func(sl *models.Slug) error {
		qAdd := "INSERT INTO sms.link (user_id, slug_id, expired) VALUES ($1, $2, $3) ON CONFLICT DO NOTHING"
		_, err := client.Exec(context.Background(), qAdd, user.Id, sl.Id, sl.ExpiredDt)
		return err
	}
	funDel := func(sl *models.Slug) error {
		qDel := "DELETE FROM sms.link WHERE user_id=$1 AND slug_id=$2"
		_, err := client.Exec(context.Background(), qDel, user.Id, sl.Id)
		return err
	}
	unknownSlugs := []*models.Slug{}

	f := func(fun func(sl *models.Slug) error, slugs []*models.Slug) error {
		for i, sl := range slugs {
			if err := fillSlugId(client, sl); err != nil {
				unknownSlugs = append(unknownSlugs, slugs[i])
				continue
			}
			if err := fun(sl); err != nil {
				return err
			}
		}
		return nil
	}
	if err := f(funAdd, add); err != nil {
		return err
	}
	if err := f(funDel, del); err != nil {
		return err
	}

	if len(unknownSlugs) > 0 {
		return &SlugsNotExist{unknownSlugs}
	}
	return nil
}

func GetUserSlugs(client PostgresClient, user models.User) ([]*models.Slug, error) {
	q := `
SELECT l.slug_id AS Id, s.name AS Name
FROM sms.link l
JOIN sms.slug s ON s.id = l.slug_id
WHERE l.user_id = $1
`

	var slugs []*models.Slug
	err := pgxscan.Select(context.Background(), client, &slugs, q, user.Id)
	return slugs, err
}

type HistoryRow struct {
	UserId    int
	SlugName  string
	Operation string
	Datetime  time.Time
}

func GetHistory(client PostgresClient, from time.Time) ([]*HistoryRow, error) {
	q := `
SELECT h.user_id, s.name AS slug_name, h.operation, h.datetime
FROM sms.history h
LEFT JOIN sms.slug s ON h.slug_id = s.id
WHERE h.datetime >= $1
ORDER BY h.datetime DESC
`

	var history []*HistoryRow
	err := pgxscan.Select(context.Background(), client, &history, q, from)
	return history, err
}
