package postgres

import (
	"fmt"
	"sms/models"
)

type SlugAlreadyExist struct {
	sl *models.Slug
}

func (e SlugAlreadyExist) Error() string { return fmt.Sprintf("slug %s already exists", e.sl.Name) }

type SlugsNotExist struct {
	sl []*models.Slug
}

func (e SlugsNotExist) Error() string {
	str := "slugs"
	for _, sl := range e.sl {
		str = fmt.Sprintf("%s %s", str, sl.Name)
	}
	return fmt.Sprintf("%s not exist", str)
}

type ErrNoRows struct{}

func (e ErrNoRows) Error() string { return "no rows in result set" }
