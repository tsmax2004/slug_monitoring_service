package main

import (
	"github.com/gin-gonic/gin"
	"log"
)

func setupRouter() *gin.Engine {
	server, err := CreateServer()
	if err != nil {
		log.Fatal(err)
	}

	router := gin.Default()
	router.POST("/slugs", server.postSlug)
	router.DELETE("/slugs", server.deleteSlug)
	router.PUT("/link", server.updateUserSlugs)
	router.GET("/link", server.getUserSlugs)
	router.GET("/history", server.getHistory)
	return router
}

func main() {
	router := setupRouter()
	router.Run(":8080")
}
