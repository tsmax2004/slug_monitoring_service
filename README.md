# SlugMonitoringService ⚙️

Решение [тестового задания для стажёра Backend AvitoTech](https://github.com/avito-tech/backend-trainee-assignment-2023).



## How to run 🏃

```bash
cd slug_monitoring_service
docker-compose up
```

## API 📝

- [Swagger файл](docs/openapi.yaml)
- [Примеры запросов/ответов с помощью curl](docs/api_examples.md)

## How works ⚙️

### 1. Модели

Представлены две [модели](src/models) для связи пользователя с сервером при помощи HTTP с форматом JSON,
а также для связи сервера с базой данных.

### 2. База данных

Используется PostrgreSQL ([докер](docker-compose.yml)). При первом запуске контейнера запускаются
[DDL скрипты](db/ddl.sql), данные сохраняются и могут быть переиспользованы при повторном запуске контейнера.
Для очищения данных можно использовать флаг при остановке:

```bash
docker-compose down --volumes
```

Обращения к базе данных реализованы [здесь](src/postgres/db.go) при помощи [pgx](https://github.com/jackc/pgx).

### 3. Сервер

[Реализация](src/server.go) сервера и обработчиков запросов с использованием фреймворка [gin](https://github.com/gin-gonic/gin).
Запросы ожидаются по адресу `localhost:8080`.


## What can do ✅

> Для использования см. [Swagger файл](docs/openapi.yaml) или [примеры запросов](docs/api_examples.md).

### 1. Создание сегмента
### 2. Удаление сегмента
### 3. Обновление сегментов пользователя
### 4. Получение сегментов пользователя
### 5. Логирование изменений в сегментах пользователей
Реализовано при помощи триггеров БД.
### 6. Возможность задавать TTL пользователя в сегменте
При добавлении пользователя в сегмент, можно указать, когда его автоматически исключить из него. Реализовано в горутине ttlCycle() с использованием таймера.
### 7. Автоматическое добавление пользователей в новый сегмент
При создании сегмента можно передать процент пользователей, которые попадут в сегмент автоматически. 
Семантика: `n%` всех зарегистрированных (состоящих в каком-то сегменте) пользователей добавляются в новый сегмент.