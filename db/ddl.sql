CREATE SCHEMA sms;

CREATE TABLE sms.slug
(
    id   UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name VARCHAR(64) NOT NULL
);

CREATE TABLE sms.link
(
    user_id INT  NOT NULL,
    slug_id UUID NOT NULL,
    expired TIMESTAMP,
    UNIQUE (user_id, slug_id),
    CONSTRAINT slug_fk FOREIGN KEY (slug_id) REFERENCES sms.slug (id)
);

CREATE TABLE sms.history
(
    user_id   INT  NOT NULL,
    slug_id   UUID NOT NULL,
    operation VARCHAR(3),
    datetime  TIMESTAMP
);


CREATE
OR REPLACE FUNCTION log()
RETURNS trigger AS $body$
BEGIN
    if (TG_OP = 'INSERT') then
        INSERT INTO sms.history (user_id, slug_id, operation, datetime)
        VALUES(NEW.user_id, NEW.slug_id, 'add', NOW());

        RETURN NEW;

    elsif (TG_OP = 'DELETE') then
        INSERT INTO sms.history (user_id, slug_id, operation, datetime)
        VALUES(OLD.user_id, OLD.slug_id, 'del', NOW());

        RETURN OLD;
    end if;

END;
$body$
LANGUAGE plpgsql;

CREATE TRIGGER logging
    AFTER INSERT OR DELETE
    ON sms.link
    FOR EACH ROW
    EXECUTE FUNCTION log();
